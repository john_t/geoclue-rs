/// No-op.
macro_rules! skip_assert_initialized {
    () => {};
}

macro_rules! assert_initialized_main_thread {
    () => {
        #[cfg(feature = "gtk")]
        {
            if !::gtk::is_initialized_main_thread() {
                if ::gtk::is_initialized() {
                    panic!("GTK may only be used from the main thread.");
                } else {
                    panic!("GTK has not been initialized. Call `gtk::init` first.");
                }
            }
        }
        #[cfg(feature = "gtk4")]
        {
            if !::gtk4::is_initialized_main_thread() {
                if ::gtk4::is_initialized() {
                    panic!("GTK may only be used from the main thread.");
                } else {
                    panic!("GTK has not been initialized. Call `gtk::init` first.");
                }
            }
        }
    };
}
