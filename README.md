# Geoclue

[Geoclue](https://gitlab.freedesktop.org/geoclue/geoclue) is a Dbus library for geo location.

Geoclue is used in GNOME Maps.
